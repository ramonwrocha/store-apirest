package com.store.apirest.controllers;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.store.apirest.models.Produto;
import com.store.apirest.repository.ProdutoRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value="/api/produto")
@Api(value="API REST Produtos")
@CrossOrigin(origins="*")
public class ProdutoController {
	
	@Autowired
	ProdutoRepository repository;
	
	@GetMapping
	@ApiOperation(value="Retorna todos os produtos")
	public List<Produto> getAllProdutos() {
		return repository.findAll();
	}
	
	@GetMapping("{id}")
	@ApiOperation(value="Retorna um produto por id")
	public Produto getProduto(@PathVariable(value="id") long id) {
		return repository.findById(id);
	}
	
	@PostMapping
	@ApiOperation(value="Add um novo produto")
	public Produto saveProduto(@RequestBody Produto model) {
		return repository.save(model);
	}
	
	@PutMapping
	@ApiOperation(value="Atualiza um produto por id")
	public Produto updateProduto(@RequestBody Produto model) {
		return repository.save(model);
	}
	
	@DeleteMapping("{id}")
	@ApiOperation(value="Remove um produto por id")
	public void deleteProduto(@PathVariable(value="id") long id) {
		Produto model = repository.findById(id);
		repository.delete(model);		
	}
	
}
